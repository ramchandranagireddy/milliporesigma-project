import { Component, OnInit, OnDestroy } from '@angular/core';
import { EquipmentService } from './components/equipment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(private equipmentService: EquipmentService) { }
  title = 'lab';
  equipments = [];

  currentEquipment = {
    name: '',
    vendor: '',
    location: '',
    model: '',
    serial: '',
    description: ''
  };

  filterValue(obj, key, value) {
    return obj.find(function (v) { return v[key] === value; });
  }

  updateEquipment(equipment) {
    this.currentEquipment = this.filterValue(this.equipments, 'name', equipment);
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.equipmentService.getEquipments()
      .subscribe((data: any) => {
        this.equipments = data;
      });
  }

}
