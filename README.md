# Assignment
This project comprises of two code bases, one for server and other for UI. Please find the details of setup below:

### Prerequisites:
- Java 8 or later
- Node JS 8

## Server
The server side code is written in spring boot. This exposes a REST service to return the equipments. The service exposed is as follows:
> http://localhost:8080/equipments

```json

[
  {
    "name": "Equipment1",
    "vendor": "Vendor1",
    "location": "Location1",
    "model": "Model1",
    "serial": "Serial1",
    "description": "Vendor1 Desc",
    "url": null
  },
  {
    "name": "Equipment2",
    "vendor": "Vendor2",
    "location": "Location2",
    "model": "Model2",
    "serial": "Serial2",
    "description": "Vendor2 Desc",
    "url": null
  }
  .....
]
```
Please find the installation steps below:

```sh
$ Navigate to UI directory
$ Import the project in eclipse as a maven project
$ Run Maven build
$ Start the spring boot application by running the com.lab.spring.SpringBootRestApplication java class.
```
> Note: If maven is installed the project can be build in terminal as well.

## UI
The UI is build on Angular 7. This application consumes the REST service provided as part of the server. Setup steps:
```sh
$ Navigate to UI directory
$ Open the directory in a terminal
$ Run : npm install
$ Run : npm start to start the application
```