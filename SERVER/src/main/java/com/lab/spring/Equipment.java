package com.lab.spring;

import org.springframework.stereotype.Component;

public class Equipment {

	Equipment(String name, String vendor, String location, String model, String serial, String description, String url) {
		this.name = name;
		this.vendor = vendor;
		this.location = location;
		this.model = model;
		this.serial = serial;
		this.description = description;
		this.url = url;
	}
	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	private String name;
	private String vendor;
	private String location;
	private String model;
	private String serial;
	private String description;
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
